//--------------------------------------------------------------------------------
// Arduboy pong tutorial
// Dave Astels 2018
// MIT License  see LICENSE.md
//--------------------------------------------------------------------------------

#include <Arduboy.h>

Arduboy arduboy;

int game_state = 0;
int justpressed = 0;

int ball_x = 62;
int ball_y = 0;
int ball_size = 4;
int ball_right = 1;
int ball_down = 1;

int paddle_width = 4;
int paddle_height = 9;
int player_x = 0;
int player_y = 0;
int computer_y = 0;
int computer_x = 127 - paddle_width;

int player_score = 0;
int computer_score = 0;

void setup()
{
  arduboy.begin();
  srand(7/8);
  arduboy.setFrameRate(60);
  arduboy.clear();
}


void reset_game() {
  ball_x = 63;
  player_score = 0;
  computer_score = 0;
}


void loop()
{
  if(!arduboy.nextFrame()) {
	return;
  }
  arduboy.clear();
  switch (game_state ) {
  case 0:
    //Title screen
    reset_game();
    arduboy.setCursor(0, 0);
    arduboy.print("Title Screen");
    if (arduboy.pressed(A_BUTTON) && !justpressed) {
      justpressed = true;
      game_state = 1;
    }
    break;
  case 1:
    //Gameplay screen

    arduboy.setCursor(20, 0);
    arduboy.print(player_score);
    
    arduboy.setCursor(101, 0);
    arduboy.print(computer_score);
    
    // player paddle
    arduboy.fillRect(player_x, player_y, paddle_width, paddle_height, WHITE);
    if (arduboy.pressed(UP_BUTTON) && player_y > 0) {
      player_y--;
    }
    if (arduboy.pressed(DOWN_BUTTON) && player_y + paddle_height< 63) {
      player_y++;
    }

    // computer paddle
    arduboy.fillRect(computer_x, computer_y, paddle_width, paddle_height, WHITE);
    if (ball_x > 115 || rand() % 20 == 1) {
      if (ball_y < computer_y) {
        computer_y--;
      }
      if (ball_y + ball_size > computer_y + paddle_height) {
        computer_y++;
      }
    }

    // ball
    arduboy.fillRect(ball_x, ball_y, ball_size, ball_size, WHITE);
    ball_x += ball_right;
    ball_y += ball_down;
    if (ball_x == player_x + paddle_width && player_y < ball_y + ball_size && player_y + paddle_height > ball_y) {
      ball_right = 1;
    }
    if (ball_x + ball_size == computer_x && computer_y < ball_y + ball_size && computer_y + paddle_height > ball_y) {
      ball_right = -1;
    }
 
    if (ball_y == 0) {
      ball_down = 1;
    }
    if (ball_y + ball_size == 63) {
      ball_down = -1;
    }

    if (ball_x < -10) {
      ball_x = 63;
      computer_score++;
    }
    if (ball_x > 130) {
      ball_x = 63;
      player_score++;
    }

    if (player_score == 5) {
      game_state = 2;
    }
    if (computer_score == 5) {
      game_state = 3;
    }

    break;
  case 2:
    //Win screen
    arduboy.setCursor(0, 0);
    arduboy.print("Win Screen");
    if (arduboy.pressed(A_BUTTON) && !justpressed) {
      justpressed = true;
      game_state = 0;
    }
    break;
  case 3:
    //Game over screen
    arduboy.setCursor(0, 0);
    arduboy.print("Game Over Screen");
    if (arduboy.pressed(A_BUTTON) && !justpressed) {
      justpressed = true;
      game_state = 0;
    }
    break;
  }
  if (arduboy.notPressed(A_BUTTON)) {
	justpressed = false;
  }
  arduboy.display();
}